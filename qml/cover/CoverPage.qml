import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover

    Label {
        x: Theme.paddingLarge
        y: Theme.paddingLarge
        color: Theme.highlightColor
        font.pixelSize: Theme.fontSizeSmall
        text: 'Pusfofefe'
    }

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.highlightColor
            text: g_cover_messages + ' messages'
        }

        Label {
            anchors.horizontalCenter: parent.horizontalCenter
            color: Theme.primaryColor
            text: g_cover_new_messages + ' new'
        }
    }

    CoverActionList {
        CoverAction {
            iconSource: 'image://theme/icon-cover-sync'
            onTriggered: refreshMessages.start()
        }
    }
}
