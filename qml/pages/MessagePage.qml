import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: messagePage

    property var message: {}
    property int messageIndex: -1

    RemorsePopup { id: remorse }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: column.height

        // FIXME this causes warnings on the CLI since `message` becomes `null`
        PullDownMenu {
            MenuItem {
                text: "Delete"
                onClicked: remorse.execute("Deleting message",
                    function() {
                        if (messagesJSON.model.count == 1) {
                            pageStack.navigateBack();
                        } else if (messagesJSON.model.count == 2 && messageIndex == 1) {
                            pageStack.replace(
                                Qt.resolvedUrl('MessagePage.qml'),
                                { message: messagesJSON.model.get(0),
                                  messageIndex: 0 });
                        } else {
                            pageStack.replace(
                                Qt.resolvedUrl('MessagePage.qml'),
                                { message: messagesJSON.model.get(messageIndex + 1),
                                  messageIndex: messageIndex });
                        }
                        deleteMessage(messageIndex);
                    })
            }
        }

        VerticalScrollDecorator {}

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader {
                title: messageIndex
            }

            DetailItem {
                width: parent.width
                label: "App"
                value: message.app
            }

            DetailItem {
                width: parent.width
                label: "ID"
                value: message.id
            }

            DetailItem {
                width: parent.width
                label: "Date"
                value: tsToDate(message.date)
            }

            Label {
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                }
                width: parent.width - (2 * Theme.paddingLarge)
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                text: message.message
            }
        }
    }

    // Does not work for IconButton, FFS.
    ButtonLayout {
        //width: parent.width - 2 * Theme.horizontalPageMargin
        //x: Theme.horizontalPageMargin
        y: parent.height - childrenRect.height - Theme.paddingLarge

        Button {
        //IconButton {
            color: Theme.highlightColor
            text: "<"
            //icon.source: "image://theme/icon-m-back"
            onClicked: pageStack.replace(
                Qt.resolvedUrl("MessagePage.qml"),
                { message: messagesJSON.model.get(messageIndex -1),
                  messageIndex: messageIndex - 1 })
            enabled: messageIndex > 0
        }

        Button {
        //IconButton {
            color: Theme.highlightColor
            text: ">"
            //icon.source: "image://theme/icon-m-forward"
            onClicked: pageStack.replace(
                Qt.resolvedUrl("MessagePage.qml"),
                { message: messagesJSON.model.get(messageIndex + 1),
                  messageIndex: messageIndex + 1 })
            enabled: messageIndex < (messagesJSON.model.count - 1)
        }
    }
}
