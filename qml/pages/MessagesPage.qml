import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: messagesPage

    RemorsePopup { id: remorse }

    BusyLabel {
        id: busyIndicator
        anchors.centerIn: parent
        running: refreshMessages.running
        text: "Loading messages..."
    }

    SilicaListView {
        id: messagesList
        anchors.fill: parent
        model: messagesJSON.model

        header: PageHeader { title: 'Messages' }

        delegate: ListItem {
            id: messageEntry

            Label {
                id: messageLabel
                anchors {
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                }
                width: parent.width - (2 * Theme.paddingLarge)
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                text: model.message
            }

            Label {
                anchors {
                    top: messageLabel.bottom
                    left: parent.left
                    leftMargin: Theme.paddingLarge
                }
                width: (parent.width - (2 * Theme.paddingLarge)) / 2
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                font.pixelSize: Theme.fontSizeExtraSmall
                text: tsToDate(model.date)
            }

            Label {
                anchors {
                    top: messageLabel.bottom
                    right: parent.right
                    rightMargin: Theme.paddingLarge
                }
                width: (parent.width - (2 * Theme.paddingLarge)) / 2
                truncationMode: TruncationMode.Fade
                maximumLineCount: 1
                font.pixelSize: Theme.fontSizeExtraSmall
                text: model.app
            }

            onClicked: pageStack.push(Qt.resolvedUrl('MessagePage.qml'),
                                      { message: messagesJSON.model.get(index),
                                        messageIndex: index })

            menu: ContextMenu {
                MenuItem {
                    text: "Delete"
                    onClicked: messageEntry.remorseAction("Deleting message",
                        function() { deleteMessage(index); })
                }
            }
        }

        Component.onCompleted: {
            log('messagesList completed');
        }

        PullDownMenu {
            MenuItem {
                text: 'Settings'
                onClicked: pageStack.push(Qt.resolvedUrl('SettingsPage.qml'))
            }
            MenuItem {
                text: 'Clear All Messages'
                onClicked: remorse.execute('Deleting all messages',
                                           deleteMessages);
            }
            MenuItem {
                text: 'Refresh'
                onClicked: refreshMessages.start()
            }
        }

        VerticalScrollDecorator {}

        ViewPlaceholder {
            enabled: messagesList.count == 0
            text: 'No messages'
            hintText: 'Pull down to refresh'
        }
    }

    // Functions

    function deleteMessages () {
        log('deleting all messages');
        messagesJSON.model.clear();
        g_cover_new_messages = 0;
        writeMessages();
    }
}
