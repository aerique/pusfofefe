import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.KeepAlive 1.2

Page {
    RemorsePopup { id: remorse }

    BusyLabel {
        id: busyIndicator
        anchors.centerIn: parent
        running: loginAndRegister.running
        text: "Registering device..."
    }

    SilicaFlickable {
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                text: 'About'
                onClicked: pageStack.push(Qt.resolvedUrl('AboutPage.qml'))
            }
            MenuItem {
                text: 'Reset Settings'
                onClicked: remorse.execute('Resetting settings to defaults',
                                           resetSettings)
            }
        }

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader { title: 'Settings' }

            SectionHeader { text: 'Pushover' }

            TextField {
                id: pushoverEmail
                label: 'E-mail Address'
                placeholderText: label
                text: g_email
                onTextChanged: g_email = pushoverEmail.text
            }

            PasswordField {
                id: pushoverPassword
                text: g_password
                onTextChanged: g_password = pushoverPassword.text
            }

            Button {
                width: parent.width - 2 * Theme.horizontalPageMargin
                x: Theme.horizontalPageMargin
                text: 'Register Device'
                onClicked: loginAndRegister.start()
            }

            Label {
                width: parent.width - 2 * Theme.horizontalPageMargin
                x: Theme.horizontalPageMargin
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
                onLinkActivated: Qt.openUrlExternally(link)
                text: '<style>a:link { color: ' + Theme.primaryColor + '; }</style>(To sign up for Pushover please go to: <a href="https://pushover.net/signup">https://pushover.net/signup</a>.)'
            }

            SectionHeader { text: 'Miscellaneous' }

            ComboBox {
                id: pushoverRefresh
                width: parent.width
                description: 'between Pushover checks'
                currentIndex: switch (g_refresh) {
                    case BackgroundJob.FiveMinutes:    return 0;
                    case BackgroundJob.FifteenMinutes: return 1;
                    case BackgroundJob.ThirtyMinutes:  return 2;
                    case BackgroundJob.OneHour:        return 3;
                    case BackgroundJob.FourHours:      return 4;
                    default: return 2;
                }
                // The index and menu isn't all very nice, but it'll
                // suffice for now. Changing any of these requires
                // updating three objects: currentIndex, MenuItem and
                // onClicked switch.
                menu: ContextMenu {
                    MenuItem { text: '5 minutes'  }
                    MenuItem { text: '15 minutes' }
                    MenuItem { text: '30 minutes' }
                    MenuItem { text: '1 hour'     }
                    MenuItem { text: '4 hours'    }
                    onClicked: {
                        switch (pushoverRefresh.currentIndex) {
                            case 0: g_refresh = BackgroundJob.FiveMinutes;
                                                break;
                            case 1: g_refresh = BackgroundJob.FifteenMinutes;
                                                break;
                            case 2: g_refresh = BackgroundJob.ThirtyMinutes;
                                                break;
                            case 3: g_refresh = BackgroundJob.OneHour;
                                                break;
                            case 4: g_refresh = BackgroundJob.FourHours;
                                                break;
                            default: g_refresh = BackgroundJob.ThirtyMinutes;
                        }
                        saveSettings();
                    }
                }
            }
        }
    }

    Timer {
        id: loginAndRegister
        interval: 128  // see README
        onTriggered: {
            var loginResult = login();
            if (loginResult.success) {
                var registerResult = register();
                if (registerResult.success) {
                    saveSettings();
                } else {
                    //showFeedback(registerResult.msg);
                    // This message isn't necessarily correct but I'm done
                    // figuring out why `showFeedback` won't work.
                    feedbackLabel.text = 'Device already registered';
                    feedback.visible = true;
                }
            } else {
                //showFeedback(loginResult.msg);
                // This message isn't necessarily correct but I'm done
                // figuring out why `showFeedback` won't work.
                feedbackLabel.text = 'Invalid credentials';
                feedback.visible = true;
            }
        }
    }

    // Functions

    function login () {
        log('logging in');
        var xhr = new XMLHttpRequest();
        xhr.open('POST', g_api + 'users/login.json', false);
        xhr.setRequestHeader('User-Agent', g_app + ' ' + g_version);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var body = 'email=' + g_email + '&password=' + g_password;
        xhr.send(body);
        try {
            var json = JSON.parse(xhr.responseText);
            if (json.status == 1) {
                g_secret = json.secret;
                return { 'success': true, 'msg': '' };
            } else {
                log(xhr.responseText);
                return { 'success': false, 'msg': xhr.responseText };
            }
        } catch (e) {
            log(e);
            return { 'success': false, 'msg': e };
        }
        return { 'success': false, 'msg': 'unknown (fell through)' };
    }

    function register () {
        log('registering device');
        var xhr = new XMLHttpRequest();
        xhr.open('POST', g_api + 'devices.json', false);
        xhr.setRequestHeader('User-Agent', g_app + ' ' + g_version);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var body = 'secret=' + g_secret + '&name=' + g_app + '&os=O';
        xhr.send(body);
        try {
            var json = JSON.parse(xhr.responseText);
            if (json.status == 1) {
                g_device = json.id;
                return { 'success': true, 'msg': '' };
            } else {
                log(xhr.responseText);
                return { 'success': false, 'msg': xhr.responseText };
            }
        } catch (e) {
            log(e);
            return { 'success': false, 'msg': e };
        }
        return { 'success': false, 'msg': 'unknown (fell through)' };
    }

    function resetSettings () {
        g_email    = '';
        g_password = '';
        g_secret   = '';
        g_device   = '';
        pushoverRefresh.currentIndex = 0;
    }

    function saveSettings () {
        log('writing config to ' + g_config_file);
        writeToFile(g_config_file, { 'email':    g_email,
                                     'password': g_password,
                                     'secret':   g_secret,
                                     'device':   g_device,
                                     'refresh':  g_refresh });
    }
}
