import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    PageHeader {
        id: pageHeader
        title: 'About'
    }

    SilicaFlickable {
        width: parent.width
        anchors {
            top: pageHeader.bottom
            topMargin: Theme.paddingLarge
            left: parent.left
            leftMargin: Theme.paddingLarge
            right: parent.right
            rightMargin: Theme.paddingLarge
        }

        VerticalScrollDecorator {}

        Column {
            width: parent.width
            height: childrenRect.height
            spacing: Theme.paddingLarge

            Label {
                width: parent.width
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
                textFormat: Text.RichText
                onLinkActivated: Qt.openUrlExternally(link)

                text:
                '<style>a:link { color: ' + Theme.primaryColor + ' }</style>' +

                '<p>Pusfofefe is a Pushover client for Sailfish OS written by Erik Winkels &lt;<a href="mailto:aerique@xs4all.nl">aerique@xs4all.nl</a>&gt;.</p>' +

                '<p>&nbsp;</p>' +

                '<p>The source code is available at: <a href="https://codeberg.org/aerique/pusfofefe">https://codeberg.org/aerique/pusfofefe</a></p>' +

                '<p>&nbsp;</p>' +

                '<p>App icon made by <a href="https://www.flaticon.com/authors/freepik">Freepik</a> from <a href="https://www.flaticon.com/">www.flaticon.com</a>.</p>'
            }
        }
    }
}
