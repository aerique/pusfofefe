import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.KeepAlive 1.2

import 'components/'

import 'pages/' as Pages

ApplicationWindow
{
    id: appwin

    property string g_app: 'Pusfofefe'
    property string g_version: '3.0.1'

    property bool g_verbose: true  // disable on final 3.0.0 release
    property bool g_app_active: Qt.application.state == Qt.ApplicationActive

    property string g_api: 'https://api.pushover.net/1/'

    // These 5 are stored in `config.json`.
    property string g_email:    ''
    property string g_password: ''
    property string g_secret:   ''
    property string g_device:   ''
    property int    g_refresh:  BackgroundJob.FifteenMinutes

    property string g_messages: '{"messages":[]}'
    property var    g_message_highest_id: 0  // int will overflow here
    property var    g_message_old_msg_id: 0  // int will overflow here
    property int    g_cover_messages: messagesJSON.model.count
    property int    g_cover_new_messages: 0

    property string g_config_file: StandardPaths.data + '/config.json'
    property string g_messages_file: StandardPaths.cache + '/messages.json'

    initialPage: Component { Pages.MessagesPage { } }
    cover: Qt.resolvedUrl('cover/CoverPage.qml')
    allowedOrientations: defaultAllowedOrientations

    Component.onCompleted: {
        log('appwin completed');
        log('reading config from ' + g_config_file);
        try {
            loadSettings();
        } catch (e) {
            log('no config found (' + e + ')');
        }
        log('reading messages from ' + g_messages_file);
        try {
            loadMessages();
        } catch (e) {
            log('no messages found (' + e + ')');
        }
        refreshMessages.start()
    }

    Component.onDestruction: log('appwin destroyed')

    onActiveFocusChanged: log('focus changed')

    // Components

    JSONListModel {
        id: messagesJSON
        json: g_messages
        query: '$.messages'
    }

    BackgroundItem {
        id: feedback
        anchors.fill: parent
        visible: false

        Rectangle {
            property bool portrait: appwin.orientation == Orientation.PortraitMask

            width: parent.width / 1.5
            height: portrait ? parent.width / 1.5 : parent.width / 3
            x: parent.width / 6
            y: portrait ? (parent.height / 2) - (parent.width / 3) :
                          (parent.height / 2) - (parent.width / 6)
            radius: portrait ? parent.width / 16 : parent.height /16

            color: Theme.overlayBackgroundColor
            opacity: Theme.opacityOverlay

            Icon {
                id: warningIcon
                y: Theme.paddingLarge
                anchors.horizontalCenter: parent.horizontalCenter
                source: 'image://theme/icon-l-weather-d200-light'
            }

            Label {
                id: feedbackLabel
                width: parent.width - 2 * Theme.paddingLarge
                anchors.top: warningIcon.bottom
                x: Theme.paddingLarge
                color: Theme.secondaryHighlightColor
                wrapMode: Text.WordWrap
                text: ''
            }
        }

        onClicked: visible = false
    }

    BackgroundJob {
        id: regularRefresh
        frequency: g_refresh
        enabled: true
        onTriggered: {
            log('regular refresh triggered');
            refreshMessages.start();
        }
    }

    Timer {
        id: refreshMessages
        interval: 128  // see README
        onTriggered: {
            regularRefresh.finished();  // reset regular refresh
            var result = retrieveMessages();
            if (result.success) {
                g_message_old_msg_id = g_message_highest_id;
                g_cover_new_messages = 0;
                for (var i = 0 ; i < result.messages.length ; i++) {
                    if (messagesJSON.model.count == 0 || result.messages[i].id > messagesJSON.model.get(0).id) {
                        messagesJSON.model.insert(0, result.messages[i]);
                        g_message_highest_id = result.messages[i].id;
                        g_cover_new_messages++;
                    }
                }
                writeMessages();
                log('g_message_highest_id=' + g_message_highest_id);
                log('g_message_old_msg_id=' + g_message_old_msg_id);
                updateHighestMessage();
            // Kinda gross, but meh...
            } else if (result.msg.indexOf('\"secret\":\"invalid\"') != -1) {
                feedbackLabel.text = 'Device not registered or credentials invalid';
                feedback.visible = true;
            } else {
                log('result=' + result.msg);
                feedbackLabel.text = result.msg;
                feedback.visible = true;
            }
        }
    }

    // Functions
    //
    // These are common functions usable by the whole app.
    // Each separate page can have its own private functions.

    // So I can't call this from another page for some reason. `msg` gets
    // converted to an object and shit stops working.
    //function showFeedback (msg) {
    //    if (msg.startsWith('invalid email and/or invalid password')) {
    //        feedbackLabel.text = 'Invalid credentials';
    //    } else if (msg.startsWith('has already been taken')) {
    //        feedbackLabel.text = 'Device already registered';
    //    } else {
    //        feedbackLabel.text = msg;
    //    }
    //    feedback.visible = true;
    //}

    function log (str_or_obj) {
        if (g_verbose) { console.log(JSON.stringify(str_or_obj)); }
    }

    function loadSettings () {
        var config = JSON.parse(readFromFile(g_config_file));
        g_email    = config.email;
        g_password = config.password;
        g_secret   = config.secret;
        g_device   = config.device;
        g_refresh  = config.refresh;
    }

    function loadMessages () {
        g_messages = readFromFile(g_messages_file);
        g_message_highest_id = messagesJSON.model.get(0).id;
        g_message_old_msg_id = g_message_highest_id;
        log('g_message_highest_id=' + g_message_highest_id);
        log('g_message_old_msg_id=' + g_message_old_msg_id);
    }

    function readFromFile (file) {
        var req = new XMLHttpRequest();
        req.open('GET', 'file://' + file, false);
        req.send();
        return req.responseText;
    }

    function writeToFile (file, str_or_obj) {
        var req = new XMLHttpRequest();
        req.open('PUT', 'file://' + file, false);
        req.send(JSON.stringify(str_or_obj) + '\n');
        return req.status;
    }

    function pad (number) {
        if (number < 10) {
            return '0' + number;
        } else {
            return number;
        }
    }

    function tsToDate (timestamp) {
        var d = new Date(timestamp * 1000);
        return d.getFullYear()       + '-' +
               pad(d.getMonth() + 1) + '-' +
               pad(d.getDate())      + ' ' +
               pad(d.getHours())     + ':' +
               pad(d.getMinutes())   + ':' +
               pad(d.getSeconds())
    }

    function updateHighestMessage () {
        log('updating highest message');
        var xhr = new XMLHttpRequest();
        xhr.open('POST', g_api + 'devices/' + g_device + '/update_highest_message.json', false);
        xhr.setRequestHeader('User-Agent', g_app + ' ' + g_version);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        var body = 'secret=' + g_secret + '&message=' + g_message_highest_id;
        xhr.send(body);
        try {
            var json = JSON.parse(xhr.responseText);
            if (json.status == 1) {
                return { 'success': true, 'msg': '' };
            } else {
                log(xhr.responseText);
                return { 'success': false, 'msg': xhr.responseText };
            }
        } catch (e) {
            log(e);
            return { 'success': false, 'msg': e };
        }
        return { 'success': false, 'msg': 'unknown (fell through)' };
    }

    function retrieveMessages () {
        log('retrieving messages');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', g_api + 'messages.json?secret=' + g_secret + '&device_id=' + g_device, false);
        xhr.setRequestHeader('User-Agent', g_app + ' ' + g_version);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send();
        try {
            var json = JSON.parse(xhr.responseText);
            if (json.status == 1) {
                return { 'success': true, 'messages': json.messages };
            } else {
                log(xhr.responseText);
                return { 'success': false, 'msg': xhr.responseText };
            }
        } catch (e) {
            log(e);
            return { 'success': false, 'msg': e };
        }
        return { 'success': false, 'msg': 'unknown (fell through)' };
    }

    function writeMessages () {
        var messages = []
        for (var i = 0 ; i < messagesJSON.model.count ; i++) {
            messages.push(messagesJSON.model.get(i));
        }
        log('writing messages to ' + g_messages_file);
        writeToFile(g_messages_file, { messages: messages });
    }

    function deleteMessage (index) {
        log('deleting message ' + index);
        if (messagesJSON.model.get(index).id > g_message_old_msg_id) {
            g_cover_new_messages--;
        }
        messagesJSON.model.remove(index);
        writeMessages();
    }
}
