# Pusfofefe

- **status**: maintained
- [Changelog](CHANGELOG.md)

A Pushover client written in Common Lisp.

The canonical home page of this project is
https://codeberg.org/aerique/pusfofefe

(This project is also pushed to GitLab and Sourcehut but those sites are
not monitored for support.)

## Known Issues

- Regular refreshes once the phone is in deep sleep do not work.
- Only one remorse timer can be active when deleting individual messages.
  The remorse timer for the first message to be deleted will be overridden
  by the second message's remorse timer and the first message will not be
  deleted.
- No Pushover websocket / "push" support. So when there are new messages one
  Pushover server it can take up to the refresh time defined in settings
  before they're retrieved by the Pusfofefe app.

## Backup

There's two files if you need to backup your data, they're defined in the
code by `StandardPaths.cache` and `StandardPaths.data` and in Sailfish OS
point to:

- `$HOME/.cache/net.aerique/pusfofefe/`
- `$HOME/.local/share/net.aerique/pusfofefe/`

You can replace these files if the program is not running.

## Build

### Dependencies

- [SailfishOS Builds in Docker](https://codeberg.org/aerique/sfosbid)
  (SFOSBID)

### Instructions

The project was build using [SFOSBID](https://codeberg.org/aerique/sfosbid).

Refer to that project's README on how to get it running.

The actual build steps for Pusfofefe are:

Initial steps:

- copy `Dockerfile-sfosbid-project` from SFOSBID to the project root of
  Pusfofefe.
- `/path/to/sfosbid-git/bin/project-build.sh`

Steps for every dev session:

- `/path/to/sfosbid-git/bin/project-run.sh`
    - keep this terminal running
- `/path/to/sfosbid-git/bin/project-sync.sh`
    - run this in another terminal window
    - this will keep running unless interrupted by CTRL-C
    - it will rerun the build whenever one of the files listen in `git
      ls-files` is changed

## To Do

### High Priority

- [ ] fix regular refreshes

### Normal Priority

- better UX feedback on longer background actions
    - [x] loggin in and registering device
    - [x] retrieving messages

### Low Priority

*This is all nice-to-have to I will not be doing in the short term (the next
few years).*

- [x] check format and timezone of time on MessagePage
    - should be YYYY-MM-DD HH:MM:SS (ISO'ish) and UTC
- [ ] add support searching messages
- [ ] add support showing only messages of a certain type
- [ ] add support for two-factor auth
- [ ] add Pushover [websocket](https://pushover.net/api/client#websocket)
      support
    - https://support.kraken.com/hc/en-us/articles/360044504011-WebSocket-API-unexpected-disconnections-from-market-data-feeds

## FAQ

- Why 128ms for the timer?

From about 115ms and lower the BusyTimer doesn't seem to fade-in properly
and is a little darker than it should normally be (or maybe this is
intentional by the Sailfish OS designers). So I picked a nice round number a
little higher.

From about 64ms the BusyTimer doesn't show up at all. 128ms shouldn't really
be noticeable for a human, so :shrug:.

(I also tried WorkerScript but ran into issues with `sendMessage` not
firing.)

## Resources

- https://stackoverflow.com/questions/17882518/reading-and-writing-files-in-qml-qt/37460883#37460883

## Attributions

App icon by [Freepik](https://www.flaticon.com/authors/freepik) from
[www.flaticon.com](https://www.flaticon.com/).
