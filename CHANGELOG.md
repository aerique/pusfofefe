# Changelog

## 3.0.2 (?)

### Fixed

- A BackgroundJob instead of a Timer is now used for regular Pushover
  polling, so these polls should now not get suspended.
- Multiple remorse timers when deleting messages are now possible.
- Fix `X-HarbourBackup` path for config file.

## 3.0.1 (2023-12-27)

### Fixed

- Use `StandardPaths.data` for storing the config since `StandardPaths.config`
  does not actually exist.
- Make sure errors on retrieving messages are displayed.

## 3.0.0 (2023-12-22)

** Consider this an alpha release! **

My build process doesn't handle versions numbers like `3.0.0-alpha1`
correctly so I'm just calling this release `3.0.0`, but it is an initial
release after significant changes and re-architecture. If you're currently
running an earlier version that works well for you and you do not want to
act as an alpha-tester then do not install this release.

If you're upgrading from an earlier release you should remove the Pusfofefe
config and data files and start from scratch. The following command should
help locating them:

- `find ~ -name \*harbour-pusfofefe\*`

I'm kinda winging this because I think I'm the only person using this app
and also this new version shouldn't read those files anyway.

### Changes

- The config file format has changed, you cannot use the old config
- The messages file format has changed, you cannot use the old messages file
- This app now only uses QML and JavaScript so should work on all Sailfish
  OS architectures.

## 2.1 (2021-05-13)

### Added

- `aarch64` and `i486` packages.

### Changed

- Bring source in sync with [razcampagne](https://openrepos.net/user/856/programs)'s ECL and EQL5 repository.

## 2.0 (2021-03-03)

### Changed

- Use `harbour-pusfofefe` instead of `pusfofefe` as name on the system
  side of things.

## 1.1 (2021-03-02)

### Added

- Reset poll time on manual refresh.
- Naive migration from old to new poll time format.

### Changed

- Make poll times similar to Jolla apps.
    - But no 5 minute poll time! (too short)
- Replace BusyLabel with custom widget.

### Fixed

- Use BackgroundJob instead of Timer.

## 1.0 (2021-01-29)

### Changed

- Increase cover page title size.
- Make icon style more in line with Sailfish OS.
- Change application group to "Applications/Internet".
- Bring Button colors in line with theme.

### Removed

- Notifications

## 0.9 (2021-01-15)

First released version.

## Resources

- https://keepachangelog.com/
